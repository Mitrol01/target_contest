﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnable : MonoBehaviour {

    public GameObject m_prefab;
	public void Instanciate () {
        GameObject. Instantiate(m_prefab, transform.position, transform.rotation);
	}
	
}
